const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");
const misRutas = require("./router/index");
const path = require("path");

const app = express();


app.set('view engine', "ejs")
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded ({extended:true}));
app.engine('html', require('ejs').renderFile) // Cambia los js por html (Se debe cambiar el nombre a .html en la carpeta views)


app.use(bodyparser.urlencoded({extended:true}));
app.use(misRutas)



app.use((req, res, next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html');
});

// Escuchar al servidor por el puerto 3000
const puerto = 4001; // Victor
app.listen(puerto, ()=>{
    console.log("iniciando puerto 4001")
})