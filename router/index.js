const express = require("express");
const router = express.Router();
const bodyParse = require("body-parser");



router.get('/', (req, res) => {
    const resultados = {
        numRecibo:req.query.numRecibo,
        nombre:req.query.nombre,
        domicilio:req.query.domicilio,
        tipoServicio:req.query.tipoServicio,
        KilloWattsConsumidos:req.query.KilloWattsConsumidos,
        subtotal:req.query.subtotal,
        impuesto:req.query.impuesto,
        descuento:req.query.descuento,
        totalPagar:req.query.totalPagar
       
    }
    
    res.render('index.html', resultados);
  });

router.post('/', (req, res)=>{
    const resultados = {
        numRecibo:req.body.numRecibo,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
        tipoServicio:req.body.tipoServicio,
        KilloWattsConsumidos:req.body.KilloWattsConsumidos,
        subtotal:req.body.subtotal,
        impuesto:req.body.impuesto,
        descuento:req.body.descuento,
        totalPagar:req.body.totalPagar
        
    }
    res.render('index.html', resultados);
})
  
module.exports = router;